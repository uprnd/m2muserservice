#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/ModelData.o \
	${OBJECTDIR}/src/ServerApp.o \
	${OBJECTDIR}/src/ServiceFactory.o \
	${OBJECTDIR}/src/ZServiceModel.o \
	${OBJECTDIR}/src/main.o \
	${OBJECTDIR}/thrift/gen-cpp/M2MUserService.o \
	${OBJECTDIR}/thrift/gen-cpp/m2muser_constants.o \
	${OBJECTDIR}/thrift/gen-cpp/m2muser_types.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk bin/m2muserservice

bin/m2muserservice: ${OBJECTFILES}
	${MKDIR} -p bin
	${LINK.cc} -o bin/m2muserservice ${OBJECTFILES} ${LDLIBSOPTIONS} ../corelibs/UPStoreAppCommon/lib/libupstoreappcommon.a ../corelibs/UPStorage/lib/libupstorage.a ../corelibs/UPDistributed/lib/libupdistributed.a ../corelibs/UPCaching/lib/libupcaching.a ../corelibs/UPHashing/lib/libuphashing.a ../corelibs/UPBase/lib/libupbase.a ../corelibs/UPThrift/lib/libupthrift.a ../corelibs/UPPoco/lib/libuppoco.a ../corelibs/UPEvent/lib/libupevent.a ../corelibs/UPMalloc/lib/libupmalloc.a -lpthread -ldl -lrt

${OBJECTDIR}/src/ModelData.o: src/ModelData.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ModelData.o src/ModelData.cpp

${OBJECTDIR}/src/ServerApp.o: src/ServerApp.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServerApp.o src/ServerApp.cpp

${OBJECTDIR}/src/ServiceFactory.o: src/ServiceFactory.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServiceFactory.o src/ServiceFactory.cpp

${OBJECTDIR}/src/ZServiceModel.o: src/ZServiceModel.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ZServiceModel.o src/ZServiceModel.cpp

${OBJECTDIR}/src/main.o: src/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/main.o src/main.cpp

${OBJECTDIR}/thrift/gen-cpp/M2MUserService.o: thrift/gen-cpp/M2MUserService.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/M2MUserService.o thrift/gen-cpp/M2MUserService.cpp

${OBJECTDIR}/thrift/gen-cpp/m2muser_constants.o: thrift/gen-cpp/m2muser_constants.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2muser_constants.o thrift/gen-cpp/m2muser_constants.cpp

${OBJECTDIR}/thrift/gen-cpp/m2muser_types.o: thrift/gen-cpp/m2muser_types.cpp 
	${MKDIR} -p ${OBJECTDIR}/thrift/gen-cpp
	${RM} "$@.d"
	$(COMPILE.cc) -g -DDLOG -Iinc -Ithrift/gen-cpp -I../corelibs/UPStoreAppCommon/inc -I../corelibs/UPStorage/inc -I../corelibs/UPHashing/inc -I../corelibs/UPCaching/inc -I../corelibs/UPBase -I../corelibs/UPBase/upframework -I../corelibs/UPBase/inc -I../corelibs/UPPoco/inc -I../corelibs/UPThrift/inc -I../corelibs/UPBoost/inc -I../corelibs/UPEvent/inc -I../corelibs/UPThrift/inc -I../corelibs/UPThrift/thrift_src -I../corelibs/UPThrift/thrift_src/thrift -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/thrift/gen-cpp/m2muser_types.o thrift/gen-cpp/m2muser_types.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} bin/m2muserservice

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
