#pragma once

#include "storagedef.h"
#include "m2muser_types.h"
#include <cstdarg>

namespace Up {
namespace Core {
namespace M2M {
namespace I32User {

class ZServiceModel {
public:
    ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage);
public:

    void create(ErrorCode& _return, const TKey tKey, const TValue& tValue);
    void put(ErrorCode& _return, const TKey tKey, const TValue& tValue);
    void get(UserResult& _return, const TKey tKey);
    void multiget(ListUser& _return, const TKeyList& tKeyList);
    void listUser(ListUser& _return, const int32_t start, const int32_t limit);

    static void traceFunction(const std::string& strFunc) {
#ifdef DLOG
        std::cout << strFunc << " is called\n";
#endif
    }

    static void tracePagram(int num, ...) {
#ifdef DLOG
        va_list vl;
        va_start(vl, num);
        for (int i = 0; i < num; ++i) {
            std::cout << va_arg(vl, const char *);
        }
        std::cout << "\n";
#endif
    }

private:

    ZServiceModel(const ZServiceModel& orig);
    Poco::SharedPtr<PersistentStorageType> _storage;

};

#define TRACE_FUNCTION  ZServiceModel::traceFunction(__FUNCTION__)

}
}
}
}

