/*
 * File:   ModelData.h
 * Author: anhn
 *
 * Created on December 13, 2014, 9:48 AM
 */
#pragma once

#include "m2muser_types.h"
#include "ExceptionServer.h"
#include <memory>

namespace Up {
namespace Core {

namespace M2M {
namespace I32User {

//typedef TValue ModelData;

class ModelData : public TValue {
public:

    inline void copyFrom(const TValue& v) {
        (TValue&) * this = v;
    }

    inline void copyTo(TValue& v) const {
        v = *this;
    }


    static ModelData defVal;



};
}
}
}
}
