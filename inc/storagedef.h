#pragma once

#include "Storage/CachePersistent.h"
#include "Caching/AbstractCache.h"
#include "Caching/CacheFactory.h"
#include "thriftutil/TSerializer.h"
#include "Hashing/DefaultHasher.h"
#include "ModelData.h"

namespace Up
{
namespace Core
{
namespace M2M
{
namespace I32User
{

typedef Up::Storage::ObjectStorage< TKey, ModelData, TThriftSerializer<ModelData> > BackendStorageType;

typedef Up::Storage::CachePersistent< TKey, ModelData, BackendStorageType > PersistentStorageType;

typedef PersistentStorageType::CacheType CacheType;

typedef Up::Hashing::DefaultHasher HasherType;

typedef Up::Caching::BasicCacheFactory< TKey, ModelData, HasherType > CacheFactory;

}
}
}
}

