/*
 * File:   ExeptionSever.h
 * Author: tiennd
 *
 * Created on December 16, 2014, 4:09 PM
 */

#ifndef EXEPTIONSEVER_H
#define	EXEPTIONSEVER_H

#include <string>
#include "m2muser_types.h"

namespace Up {
namespace Core {
namespace M2M {
namespace I32User {

class ExceptionServer : public std::exception {
public:

    /* forward error */
    ExceptionServer(int type, const std::string& message, int line, const std::string& file)
    : _errorCode(type), _errorMessage(message), _line(line), _file(file) {
    }

    /* error by server no errorMessage */
    ExceptionServer(const std::string &nameServer, int type, int line, const std::string& file)
    : _errorCode(type), _line(line), _file(file) {
        switch (type) {
            case ErrorServer::ERR_UNKNOWN: _errorMessage = "error unknown by " + nameServer;
                return;

            case ErrorServer::ERR_ITEM_EXISTED: _errorMessage = "item existed by " + nameServer;
                return;

            case ErrorServer::ERR_NOTFOUND: _errorMessage = "item not found by " + nameServer;
                return;

            case ErrorServer::ERR_OUT_OF_RANGE: _errorMessage = "item out of range by " + nameServer;
                return;

            case ErrorServer::ERR_NULL_STORAGE: _errorMessage = "storage null by " + nameServer;
                return;

            default: _errorMessage = "error unknown by " + nameServer;
                return;
        }
    }

    virtual ~ExceptionServer() throw () {
    }

    int getError() const {
        return _errorCode;
    }

    std::string getErrorMessage() const {
        return _errorMessage;
    }

    int getLine() const {
        return _line;
    }

    std::string getFile() const {
        return _file;
    }


private:
    int _errorCode;
    std::string _errorMessage;
    int _line;
    std::string _file;
};

/* forward error */
#define EXCEPTION_SERVER(type, message) ExceptionServer(type, message,  __LINE__, __FILE__ )

/* error by server no errorMessage */
#define EXCEPTION_SERVER_TYPE(nameServer, type) ExceptionServer( std::string(nameServer), type, __LINE__, __FILE__ )

#define ERROR_SERVER  "m2muser"

}
}
}
}




#endif	/* EXEPTIONSEVER_H */

