#pragma once

#include "thriftutil/TThriftServer.h"
#include "ZServiceModel.h"
#include "M2MUserService.h"

namespace Up {
namespace Core {
namespace M2M {
namespace I32User {

class ServiceHandler : virtual public M2MUserServiceIf {
public:
    typedef up::up101::transport::TThriftServer<M2MUserServiceIf, M2MUserServiceProcessor,
    ::apache::thrift::protocol::TBinaryProtocolFactory> ThriftServer;

    ServiceHandler(Poco::SharedPtr<ZServiceModel> aModel) {
        _model = aModel;
        _pmodel = _model.get();

    };


private:
    Poco::SharedPtr<ZServiceModel> _model;
    ZServiceModel* _pmodel;

public:

    virtual void create(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
        if (_pmodel) _pmodel->create(_return, tKey, tValue);
    }

    virtual void multiget(ListUser& _return, const TKeyList& tKeyList) {
        if (_pmodel) _pmodel->multiget(_return, tKeyList);
    }

    virtual void get(UserResult& _return, const TKey tKey) {
        if (_pmodel) _pmodel->get(_return, tKey);
    }

    virtual void put(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
        if (_pmodel) _pmodel->put(_return, tKey, tValue);
    }

    virtual void remove(ErrorCode& _return, const TKey tKey) {
        // TODO
    }

    virtual void listUser(ListUser& _return, const int32_t start, const int32_t limit) {
        if (_pmodel) _pmodel->listUser(_return, start, limit);
    }


};

}
}
}
}
