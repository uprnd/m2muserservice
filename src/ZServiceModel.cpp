#include "ZServiceModel.h"
#include "ServiceFactory.h"
#include <iostream>


namespace Up {
namespace Core {
namespace M2M {
namespace I32User {

ZServiceModel::ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage) : _storage(aStorage) {
}

class create_visitor : public PersistentStorageType::data_visitor {
public:

    create_visitor(ErrorCode& return_, const I32User::TValue& tValue)
    : _return(return_), _vl(tValue) {
	// FIXME
	TRACE_FUNCTION;
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	if (value != ModelData::defVal) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_ITEM_EXISTED);
	value.copyFrom(_vl);
	return true;
    }
private:
    ErrorCode& _return;
    const I32User::TValue& _vl;
};

void ZServiceModel::create(ErrorCode& _return, const TKey tKey, const TValue& tValue) {

    try {
	if (!_storage) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	create_visitor visitor(_return, tValue);
	_storage->visit(tKey, &visitor);

    } catch (const ExceptionServer &e) {
	_return.error = e.getError();
	_return.__set_message(e.getErrorMessage());


    } catch (...) {
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	_return.error = e.getError();
	_return.__set_message(e.getErrorMessage());

    }
}

class put_visitor : public PersistentStorageType::data_visitor {
public:

    put_visitor(const I32User::TValue& tValue)
    : _vl(tValue) {
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	value.copyFrom(_vl);
	return true;
    }
private:
    const I32User::TValue& _vl;
};

void ZServiceModel::put(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
    // FIXME
    TRACE_FUNCTION;

    try {
	if (!_storage) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	put_visitor visitor(tValue);
	_storage->visit(tKey, &visitor);
	_return.error = ErrorServer::ERR_NONE;

    } catch (const ExceptionServer &e) {
	_return.error = e.getError();
	_return.__set_message(e.getErrorMessage());

    } catch (...) {
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	_return.error = e.getError();
	_return.__set_message(e.getErrorMessage());

    }
}

class get_visitor : public PersistentStorageType::data_visitor {
public:

    get_visitor(I32User::UserResult& return_)
    : _return(return_) {
	// FIXME
	TRACE_FUNCTION;
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	if (value == ModelData::defVal) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NOTFOUND);
	value.copyTo(_return.user);
	_return.__isset.user = true;
	return false;

    }
private:
    I32User::UserResult& _return;
};

void ZServiceModel::get(UserResult& _return, const TKey tKey) {
    try {
	if (!_storage) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	get_visitor visitor(_return);
	_storage->visit(tKey, &visitor);

    } catch (const ExceptionServer &e) {
	auto &error = _return.errorCode;
	error.error = e.getError();
	error.__set_message(e.getErrorMessage());


    } catch (...) {
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	auto &error = _return.errorCode;
	error.error = e.getError();
	error.__set_message(e.getErrorMessage());

    }
}

void ZServiceModel::multiget(ListUser& _return, const TKeyList& tKeyList) {

    _return.errorCode = ErrorServer::ERR_NONE;
    for (TKeyList::const_iterator iter = tKeyList.begin(); iter != tKeyList.end(); ++iter) {
	auto &userResult = _return.userResult;
	auto tKey = *iter;
	get(userResult[tKey], tKey);
    }
    if (!_return.userResult.empty()) _return.__isset.userResult = true;
}

void ZServiceModel::listUser(ListUser& _return, const int32_t start, const int32_t limit) {

    // FIXME
    TRACE_FUNCTION;

    try {

	/* get start */
	int begin = 0;
	if (start == 0) begin = 1;
	else begin = start;


	/* check list empty */
	int limitGet = ServiceFactory::limitGet;
	UserResult userResult;
	while (limitGet-- > 0) {
	    get(userResult, begin);
	    if (userResult.errorCode.error == 0) break;
	}
	if (limitGet <= 0) {
	    _return.errorCode = 0;
	    return;
	}

	/* insert to mapResult */
	std::map<TKey, UserResult > mapResult;
	mapResult[begin] = userResult;
	mapResult[begin].__isset.user = true;

	/* get list */
	UserResult result;
	for (int i = 0; i < limit; ++i) {
	    int index = begin + i;

	    int limitGet = ServiceFactory::limitGet;
	    while (limitGet-- > 0) {
		get(result, index);
		if (result.errorCode.error == 0) break;
	    }

	    mapResult[index] = result;
	    mapResult[index].__isset.user = true;
	}

	_return.errorCode = ErrorServer::ERR_NONE;
	_return.userResult = mapResult;
	_return.__isset.userResult = true;

    } catch (const ExceptionServer& e) {
	_return.errorCode = e.getError();
	_return.__set_message(e.getErrorMessage());

    } catch (...) {
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	_return.errorCode = e.getError();
	_return.__set_message(e.getErrorMessage());
    }

}



}
}
}
}
