namespace java vng.up.core.m2m.i32user
namespace cpp Up.Core.M2M.I32User


enum ErrorServer
{
    ERR_NONE = 0,   
    ERR_UNKNOWN,
    ERR_ITEM_EXISTED,
    ERR_NOTFOUND,
    ERR_OUT_OF_RANGE,
    ERR_NULL_STORAGE
}


struct ErrorCode
{
	1: i32 error,
	2: optional string message
}



struct User
{
	1: string name,
	2: string passwd,
	3: i32 createTime,
	4: i32 lastUpdate,	
	5: i32 flags = 0
}



struct UserResult
{
	1: ErrorCode errorCode,
	2: optional User user
}


typedef i32 TKey
typedef User TValue
typedef list<TKey> TKeyList


struct ListUser
{
	1: i32 errorCode,
	2: optional string message,
	3: optional map<TKey , UserResult> userResult
}

service M2MUserService
{

	ErrorCode create(1: TKey tKey, 2: TValue tValue),
	ErrorCode put(1: TKey tKey, 2: TValue tValue),
	UserResult get(1: TKey tKey),
	ListUser multiget(1:TKeyList tKeyList), 
	ErrorCode remove(1: TKey tKey),
	ListUser listUser(1: i32 start, 2: i32 limit)
}




