/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "m2muser_types.h"

#include <algorithm>

namespace Up { namespace Core { namespace M2M { namespace I32User {

int _kErrorServerValues[] = {
  ErrorServer::ERR_NONE,
  ErrorServer::ERR_UNKNOWN,
  ErrorServer::ERR_ITEM_EXISTED,
  ErrorServer::ERR_NOTFOUND,
  ErrorServer::ERR_OUT_OF_RANGE,
  ErrorServer::ERR_NULL_STORAGE
};
const char* _kErrorServerNames[] = {
  "ERR_NONE",
  "ERR_UNKNOWN",
  "ERR_ITEM_EXISTED",
  "ERR_NOTFOUND",
  "ERR_OUT_OF_RANGE",
  "ERR_NULL_STORAGE"
};
const std::map<int, const char*> _ErrorServer_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(6, _kErrorServerValues, _kErrorServerNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

const char* ErrorCode::ascii_fingerprint = "96705E9A3FD7B072319C71653E0DBB90";
const uint8_t ErrorCode::binary_fingerprint[16] = {0x96,0x70,0x5E,0x9A,0x3F,0xD7,0xB0,0x72,0x31,0x9C,0x71,0x65,0x3E,0x0D,0xBB,0x90};

uint32_t ErrorCode::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->error);
          this->__isset.error = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->message);
          this->__isset.message = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ErrorCode::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ErrorCode");

  xfer += oprot->writeFieldBegin("error", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->error);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.message) {
    xfer += oprot->writeFieldBegin("message", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->message);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ErrorCode &a, ErrorCode &b) {
  using ::std::swap;
  swap(a.error, b.error);
  swap(a.message, b.message);
  swap(a.__isset, b.__isset);
}

const char* User::ascii_fingerprint = "6A84D193C8797DF5BD46E8803DDF1208";
const uint8_t User::binary_fingerprint[16] = {0x6A,0x84,0xD1,0x93,0xC8,0x79,0x7D,0xF5,0xBD,0x46,0xE8,0x80,0x3D,0xDF,0x12,0x08};

uint32_t User::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->name);
          this->__isset.name = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->passwd);
          this->__isset.passwd = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->createTime);
          this->__isset.createTime = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->lastUpdate);
          this->__isset.lastUpdate = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->flags);
          this->__isset.flags = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t User::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("User");

  xfer += oprot->writeFieldBegin("name", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->name);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("passwd", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->passwd);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("createTime", ::apache::thrift::protocol::T_I32, 3);
  xfer += oprot->writeI32(this->createTime);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("lastUpdate", ::apache::thrift::protocol::T_I32, 4);
  xfer += oprot->writeI32(this->lastUpdate);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("flags", ::apache::thrift::protocol::T_I32, 5);
  xfer += oprot->writeI32(this->flags);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(User &a, User &b) {
  using ::std::swap;
  swap(a.name, b.name);
  swap(a.passwd, b.passwd);
  swap(a.createTime, b.createTime);
  swap(a.lastUpdate, b.lastUpdate);
  swap(a.flags, b.flags);
  swap(a.__isset, b.__isset);
}

const char* UserResult::ascii_fingerprint = "19E3EC1CA4DB8FD7CB5259B33AA78265";
const uint8_t UserResult::binary_fingerprint[16] = {0x19,0xE3,0xEC,0x1C,0xA4,0xDB,0x8F,0xD7,0xCB,0x52,0x59,0xB3,0x3A,0xA7,0x82,0x65};

uint32_t UserResult::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->errorCode.read(iprot);
          this->__isset.errorCode = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->user.read(iprot);
          this->__isset.user = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t UserResult::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("UserResult");

  xfer += oprot->writeFieldBegin("errorCode", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->errorCode.write(oprot);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.user) {
    xfer += oprot->writeFieldBegin("user", ::apache::thrift::protocol::T_STRUCT, 2);
    xfer += this->user.write(oprot);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(UserResult &a, UserResult &b) {
  using ::std::swap;
  swap(a.errorCode, b.errorCode);
  swap(a.user, b.user);
  swap(a.__isset, b.__isset);
}

const char* ListUser::ascii_fingerprint = "92257E18E88255B2266A113C746D4CA0";
const uint8_t ListUser::binary_fingerprint[16] = {0x92,0x25,0x7E,0x18,0xE8,0x82,0x55,0xB2,0x26,0x6A,0x11,0x3C,0x74,0x6D,0x4C,0xA0};

uint32_t ListUser::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->errorCode);
          this->__isset.errorCode = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->message);
          this->__isset.message = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_MAP) {
          {
            this->userResult.clear();
            uint32_t _size0;
            ::apache::thrift::protocol::TType _ktype1;
            ::apache::thrift::protocol::TType _vtype2;
            xfer += iprot->readMapBegin(_ktype1, _vtype2, _size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              TKey _key5;
              xfer += iprot->readI32(_key5);
              UserResult& _val6 = this->userResult[_key5];
              xfer += _val6.read(iprot);
            }
            xfer += iprot->readMapEnd();
          }
          this->__isset.userResult = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ListUser::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ListUser");

  xfer += oprot->writeFieldBegin("errorCode", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->errorCode);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.message) {
    xfer += oprot->writeFieldBegin("message", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->message);
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.userResult) {
    xfer += oprot->writeFieldBegin("userResult", ::apache::thrift::protocol::T_MAP, 3);
    {
      xfer += oprot->writeMapBegin(::apache::thrift::protocol::T_I32, ::apache::thrift::protocol::T_STRUCT, static_cast<uint32_t>(this->userResult.size()));
      std::map<TKey, UserResult> ::const_iterator _iter7;
      for (_iter7 = this->userResult.begin(); _iter7 != this->userResult.end(); ++_iter7)
      {
        xfer += oprot->writeI32(_iter7->first);
        xfer += _iter7->second.write(oprot);
      }
      xfer += oprot->writeMapEnd();
    }
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ListUser &a, ListUser &b) {
  using ::std::swap;
  swap(a.errorCode, b.errorCode);
  swap(a.message, b.message);
  swap(a.userResult, b.userResult);
  swap(a.__isset, b.__isset);
}

}}}} // namespace
